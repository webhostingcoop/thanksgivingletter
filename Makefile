all: clean thanksgivingLetter.docx

thanksgivingLetter.md: .BAD_POLITICIAN_NAME .GOOD_THINGS_ORG_DOES .BAD_THINGS_US_DOES .ORGANIZATION_NAME .ORGANIZATION_TYPE .ORGANIZATION_TYPE_THRESH .PATRON_NAME
	$(eval TMP := $(shell mktemp -d --suffix=DOCKERTMP))
	$(eval PATRON_NAME := "$(shell cat .PATRON_NAME)")
	$(eval ORGANIZATION := "$(shell cat .ORGANIZATION_NAME)")
	$(eval ORGANIZATION_TYPE := "$(shell cat .ORGANIZATION_TYPE)")
	$(eval ORGANIZATION_TYPE_THRESH := "$(shell cat .ORGANIZATION_TYPE_THRESH)")
	$(eval BAD_THINGS_US_DOES := "$(shell cat .BAD_THINGS_US_DOES)")
	$(eval GOOD_THINGS_ORG_DOES := "$(shell cat .GOOD_THINGS_ORG_DOES)")
	$(eval BAD_POLITICIAN_NAME := "$(shell cat .BAD_POLITICIAN_NAME)")
	PATRON_NAME=${PATRON_NAME} \
	BAD_THINGS_US_DOES=${BAD_THINGS_US_DOES} \
	GOOD_THINGS_ORG_DOES=${GOOD_THINGS_ORG_DOES} \
	BAD_POLITICIAN_NAME=${BAD_POLITICIAN_NAME} \
	ORGANIZATION=${ORGANIZATION} \
	ORGANIZATION_TYPE=${ORGANIZATION_TYPE} \
	ORGANIZATION_TYPE_THRESH=${ORGANIZATION_TYPE_THRESH} \
	envsubst < thanksgivingLetter.tpl > thanksgivingLetter.md
	cat thanksgivingLetter.md

thanksgivingLetter.docx: thanksgivingLetter.md
	pandoc -f markdown -t docx thanksgivingLetter.md -o thanksgivingLetter.docx

clean:
	-@rm thanksgivingLetter.md
	-@rm thanksgivingLetter.docx

.PATRON_NAME:
	@while [ -z "$$PATRON_NAME" ]; do \
		read -r -p "Enter the Name of the patron for whom this letter is being written to.[PATRON_NAME]: " PATRON_NAME; echo "$$PATRON_NAME">>.PATRON_NAME; cat .PATRON_NAME; \
	done ;

.ORGANIZATION_NAME:
	@while [ -z "$$ORGANIZATION_NAME" ]; do \
		read -r -p "Enter the Name of the Organization for whom this letter is being written for.[ORGANIZATION_NAME]: " ORGANIZATION_NAME; echo "$$ORGANIZATION_NAME">>.ORGANIZATION_NAME; cat .ORGANIZATION_NAME; \
	done ;

.ORGANIZATION_TYPE:
	@while [ -z "$$ORGANIZATION_TYPE" ]; do \
		read -r -p "Enter the type of the Organization for whom this letter is being written for.[ORGANIZATION_TYPE]: " ORGANIZATION_TYPE; echo "$$ORGANIZATION_TYPE">>.ORGANIZATION_TYPE; cat .ORGANIZATION_TYPE; \
	done ;

.ORGANIZATION_TYPE_THRESH:
	@while [ -z "$$ORGANIZATION_TYPE_THRESH" ]; do \
		read -r -p "Enter the type threshold of the Organization for whom this letter is being written for.[ORGANIZATION_TYPE_THRESH]: " ORGANIZATION_TYPE_THRESH; echo "$$ORGANIZATION_TYPE_THRESH">>.ORGANIZATION_TYPE_THRESH; cat .ORGANIZATION_TYPE_THRESH; \
	done ;

.GOOD_THINGS_ORG_DOES:
	@while [ -z "$$GOOD_THINGS_ORG_DOES" ]; do \
		read -r -p "Enter the mission statement of the Organization for whom this letter is being written for.[GOOD_THINGS_ORG_DOES]: " GOOD_THINGS_ORG_DOES; echo "$$GOOD_THINGS_ORG_DOES">>.GOOD_THINGS_ORG_DOES; cat .GOOD_THINGS_ORG_DOES; \
	done ;

.BAD_THINGS_US_DOES:
	@while [ -z "$$BAD_THINGS_US_DOES" ]; do \
		read -r -p "Enter the things that the United States Government does that this organization hopes to correct.[BAD_THINGS_US_DOES]: " BAD_THINGS_US_DOES; echo "$$BAD_THINGS_US_DOES">>.BAD_THINGS_US_DOES; cat .BAD_THINGS_US_DOES; \
	done ;

.BAD_POLITICIAN_NAME:
	@while [ -z "$$BAD_POLITICIAN_NAME" ]; do \
		read -r -p "Enter the Name of the Organization for whom this letter is being written for.[BAD_POLITICIAN_NAME]: " BAD_POLITICIAN_NAME; echo "$$BAD_POLITICIAN_NAME">>.BAD_POLITICIAN_NAME; cat .BAD_POLITICIAN_NAME; \
	done ;
